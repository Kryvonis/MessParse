FROM ubuntu:latest
ENV HOST=0.0.0.0

RUN \
  apt-get update && \
  apt-get install -y supervisor && \
  apt-get install -y netcat && \
  rm -rf /var/lib/apt/lists/* && \
  apt-get upgrade -y && \
  apt-get install -y  software-properties-common && \
  add-apt-repository ppa:webupd8team/java -y && \
  apt-get update && \
  echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections && \
  apt-get install -y oracle-java8-installer && \
  apt-get clean && \
  mkdir /var/www/ && \
  chown www-data:www-data /var/www/ && \
  apt-get upgrade -y && \


RUN mkdir /pip-cache
RUN chown -R www-data:www-data /pip-cache

COPY ./app /message_parse/app
COPY ./stanford-ner /message_parse/stanford-ner
COPY ./requirements.txt /message_parse/requirements.txt
COPY supervisor.conf /message_parse/supervisor.conf
COPY run.py /message_parse/run.py

RUN chown -R www-data:www-data /message_parse/

WORKDIR /message_parse
USER www-data

RUN pip3 install --user -r /message_parse/requirements.txt --cache-dir /pip-cached

RUN python -m nltk.downloader punkt
RUN python -m nltk.downloader stopwords
RUN python -m nltk.downloader averaged_perceptron_tagger

CMD ["supervisord", "-c", "/message_parse/supervisor.conf"]