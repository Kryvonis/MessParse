from flask_restplus import Api

from .main import api as main_api

api = Api(
    title='MESSAGE PARSER API',
    description='This is a test message parser',
    prefix='/api',
    doc='/doc',
    validate=True
)

api.add_namespace(main_api)
