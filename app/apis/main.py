from flask import request, jsonify, make_response
from flask_restplus import Resource, Namespace

from app.core.api_helpers import main

api = Namespace(
    'Message',
    description='namespace for message parsing',
    path='/v1/nlp')

# Custom parser
parser_msg = api.parser()
parser_msg.add_argument('message', type=str,
                        help='Type here some message that need to parse',
                        location='form')


@api.route('/parse')
class Content(Resource):
    @api.doc('Some method',
             responses={200: 'Ok',
                        400: 'Bad request'})
    @api.expect(parser_msg)
    def post(self):
        """Custom api method"""
        args = parser_msg.parse_args()
        if request.get_json():
            args['message'] = (args['message']
                               or
                               request.get_json().get('message', None))

        res = main.parse(args['message'])
        res = make_response(
            jsonify(res), res['status_code']
        )
        res.headers['content-type'] = 'application/json'
        return res
