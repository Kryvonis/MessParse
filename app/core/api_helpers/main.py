from dateutil.parser import parse as dtparse

import nltk
import usaddress
from flask import current_app
from nltk.corpus import stopwords
from nltk.tag.stanford import StanfordNERTagger

from app.settings import Config

STN = Config.APP_DIR / 'stanford-ner'

ST = StanfordNERTagger(
    str(STN / 'classifiers' / 'english.all.3class.distsim.crf.ser.gz'),
    str(STN / 'stanford-ner.jar'),
)


def parse(init_message=False, **kwargs):
    error, msg = False, 'Done'
    status_code = 200
    data = {}
    try:
        if not init_message:
            init_message = ''
        data = _parse(init_message)

    except FileNotFoundError:
        error, msg = True, 'File not found'
        status_code = 400

    except Exception as e:
        current_app.logger.info(str(e))
        error, msg = True, 'Server error'
        status_code = 500

    return {
        'error': error,
        'msg': msg,
        'init_message': init_message,
        'res': data,
        'status_code': status_code
    }


def _parse_address(sent, tags):
    """
    Simple logic for getting address
    try to find first mention of address and then cut the string
    :param sent: message that we get
    :param tags: separated word and tokenized
    :return:
    """
    # try to find state and zipcode using regexp
    zip_code = Config.ZIP_CODE_REG.findall(sent)
    state = Config.STATE_REG.findall(sent)
    min_mention = len(sent)

    if zip_code:
        min_mention = sent.index(zip_code[0])
    if state and sent.index(state[0]) < min_mention:
        min_mention = sent.index(state[0])

    # try to find latest address mention
    for tag in tags:
        if tag[1] == 'LOCATION':
            men_index = sent.index(tag[0])
            if men_index < min_mention:
                min_mention = men_index

    sent = sent[min_mention:]
    return ' '.join(sent.split(','))


def _parse_street(sent, tags):
    """
    Try to parametrize address seperate by zipcode city state
    :param sent: message that we get
    :param tags: separated word and tokenized
    :return:
    """
    res = {
        'zip_code': '',
        'state': '',
        'city': '',
        'street': '',
        'apt_num': ''
    }

    # try to find state and zipcode using regexp
    zip_code = Config.ZIP_CODE_REG.findall(sent)
    state = Config.STATE_REG.findall(sent)
    min_mention = len(sent)
    if zip_code:
        res['zip_code'] = zip_code[0]
        min_mention = sent.index(zip_code[0])
    # try to find latest address mention
    for tag in tags:
        if tag[1] == 'LOCATION':
            men_index = sent.index(tag[0])
            if men_index < min_mention:
                min_mention = men_index

    sent = sent[min_mention:]
    street = []
    city = []
    apt_num = []

    address = usaddress.parse(sent)

    for addr in address:
        if 'StreetName' in addr[1]:
            street.append(nltk.word_tokenize(addr[0])[0])
        elif 'Occupancy' in addr[1]:
            apt_num.append(nltk.word_tokenize(addr[0])[0])
        elif 'PlaceName' in addr[1]:
            city.append(nltk.word_tokenize(addr[0])[0])

    if state:
        for i, street_ in enumerate(street[:]):
            if street_ == state[0]:
                street.pop(i)

        res['state'] = state[0]

    if street:
        if not city:
            res['city'] = street.pop(0)
        res['street'] = ' '.join(street)
    if apt_num:
        res['apt_num'] = apt_num[0]

    return res


def _parse(msg: str):
    res = {
        'names': [],
        'dates': [],
        'address': ''
    }

    if not msg:
        return res

    stop = stopwords.words('english')
    document = ' '.join([i for i in msg.split() if i not in stop])

    for sent in nltk.sent_tokenize(document):
        tokens = nltk.word_tokenize(sent)
        tags = ST.tag(tokens)
        names = ' '.join([tag[0] for tag in tags if tag[1] == 'PERSON'])
        dates = ' '.join([dtparse(date).strftime('%d/%m/%Y')
                          for date in Config.DATE_REG.findall(sent)])

        # address += _parse_street(sent, tags)
        address = _parse_address(sent, tags)

        res['names'] = names
        res['dates'] = dates
        res['address'] = address

    return res
