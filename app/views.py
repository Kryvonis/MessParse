from flask import request, render_template
from app import app


@app.route('/', methods=['GET'])
def home():
    if request.method == 'GET':
        return render_template('home.html')
    elif request.method == 'POST':
        return "Bad request", 400
