import os
import re
import pathlib


class Config:
    # Flask settings
    CONFIG_NAME = 'default'

    DEBUG = False
    TESTING = False
    APP_DIR = pathlib.Path(os.path.dirname(
            os.path.dirname(os.path.abspath(__file__))
    ))

    SECRET = 'Super_secret'
    HOST = os.getenv('HOST', '127.0.0.1')
    PORT = int(os.getenv('PORT', 5000))



    # REGEXP

    DATE_REG = re.compile(
        r"""(?ix)             # case-insensitive, verbose regex
        \b                    # match a word boundary
        (?:                   # match the following three times:
         (?:                  # either
          \d{2,4}                 # a number,
          (?:\.|st|nd|rd|th)* # followed by a dot, st, nd, rd, or th (optional)
          |                   # or a month name
          (?:(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[a-z]*)
         )
         [\s./-]*             # followed by a date separator or whitespace (optional)
        ){3}                  # do this three times
        \b                    # and end at a word boundary."""
    )
    ZIP_CODE_REG = re.compile(
        '\d{5}'
    )
    STATE_REG = re.compile(
        r'(AK|Alaska|AL|Alabama|AR|Arkansas|AZ|Arizona|'
        r'CA|California|CO|Colorado|CT|Connecticut|'
        r'DC|Washington\sDC|Washington\D\.C\.|DE|Delaware|'
        r'FL|Florida|GA|Georgia|GU|Guam|HI|Hawaii|IA|Iowa|'
        r'ID|Idaho|IL|Illinois|IN|Indiana|KS|Kansas|'
        r'KY|Kentucky|LA|Louisiana|MA|Massachusetts|'
        r'MD|Maryland|ME|Maine|MI|Michigan|MN|Minnesota|'
        r'MO|Missouri|MS|Mississippi|MT|Montana|'
        r'NC|North\sCarolina|ND|North\sDakota|NE|New\sEngland|'
        r'NH|New\sHampshire|NJ|New\sJersey|NM|New\sMexico|'
        r'NV|Nevada|NY|New\sYork|OH|Ohio|OK|Oklahoma|OR|Oregon|'
        r'PA|Pennsylvania|RI|Rhode\sIsland|SC|South\sCarolina|'
        r'SD|South\sDakota|TN|Tennessee|TX|Texas|UT|Utah|'
        r'VA|Virginia|VI|Virgin\sIslands|VT|Vermont|'
        r'WA|Washington|WI|Wisconsin|WV|West\sVirginia|'
        r'WY|Wyoming)'
    )
