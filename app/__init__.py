import os
import logging
from logging.handlers import RotatingFileHandler

from flask import Flask

from app.settings import Config


def create_app(conf=None):
    app = Flask(__name__)
    if not conf:
        conf = Config
    app.config.from_object(conf)
    formatter = logging.Formatter(
        '[%(asctime)s] - %(levelname)s - %(message)s')

    if not os.path.exists(os.path.join(app.config['APP_DIR'], 'var', 'log')):
        os.makedirs(os.path.join(app.config['APP_DIR'], 'var', 'log'))
    error_handler = RotatingFileHandler(
        os.path.join(app.config['APP_DIR'], 'var', 'log', 'error.log'),
        maxBytes=10000, backupCount=1)
    error_handler.setLevel(logging.ERROR)
    error_handler.setFormatter(formatter)

    info_handler = RotatingFileHandler(
        os.path.join(app.config['APP_DIR'], 'var', 'log', 'info.log'),
        maxBytes=1000000, backupCount=10)
    info_handler.setLevel(logging.INFO)
    info_handler.setFormatter(formatter)

    app.logger.addHandler(info_handler)
    app.logger.addHandler(error_handler)

    return app


app = create_app()

_ = f'Debug: {app.debug} Testing: {app.testing}\n'
log_message = f'{_}[{app.config["CONFIG_NAME"]}] settings'
app.logger.info(log_message)

from app import views
from app.apis import api

api.init_app(app)
