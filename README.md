# Test Project for parsing structuran info from message

## Requirements

 * docker 17.09. or above
 * docker-compose 1.17 or above
 * python 3.6+


## Run app:
```
pip install -r requirements.txt
python run.py
```

```
docker-compose up
```
