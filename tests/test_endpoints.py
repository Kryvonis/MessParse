def test_simple_parse_method(app_client, user_msgs):
    for user_msg in user_msgs:
        res = app_client.post('/api/v1/nlp/parse', data={'message': user_msg})
        assert res.status_code == 200
        assert res.json
        assert res.json['init_message'] == user_msg


def test_parse_method(app_client, user_msgs, parsed_msgs):
    for user_msg, parsed_msg in zip(user_msgs, parsed_msgs):
        res = app_client.post('/api/v1/nlp/parse', data={'message': user_msg})
        assert res.status_code == 200
        assert res.json
        assert res.json['res']['names'] == parsed_msg['names']
        assert res.json['res']['dates'] == parsed_msg['dates']
        assert res.json['res']['address'] == parsed_msg['address']

