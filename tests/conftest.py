import pytest
from app.apis import api
from app import create_app


@pytest.fixture(scope="module")
def app_client():
    """
    Fixture for getting flask client
    :return:
    """
    app = create_app()
    api.init_app(app)
    client = app.test_client()
    return client


@pytest.fixture(scope="module")
def user_msgs():
    """
    Fixture for getting test message from user
    :return:
    """
    msgs = [
        "My name is Michael, I was born on December 10th 2010, and I live in California, SF",
        "My name is John Smith, I was born on 23/01/2007, and I live in Miami, FL",
        "Hi I am Artem Kryvonis, I was born on 01/01/1991, and I live in New York",
        "Hello everyone, people call me Bill Anderson, I was born on September 15th 1990, and I live in Miami, Florida",
    ]
    return msgs


@pytest.fixture(scope="module")
def parsed_msgs():
    """
    Fixture for getting test message from user
    :return:
    """
    return [
        {
            'names': 'Michael',
            'dates': '10/12/2010',
            'address': 'California  SF'
        },
        {
            'names': 'John Smith',
            'dates': '23/01/2007',
            'address': 'Miami  FL'
        },
        {
            'names': 'Artem Kryvonis',
            'dates': '01/01/1991',
            'address': 'New York'
        },
        {
            'names': 'Bill Anderson',
            'dates': '15/09/1990',
            'address': 'Miami  Florida'
        },
    ]
