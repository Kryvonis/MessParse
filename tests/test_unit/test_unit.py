from app.core.api_helpers.main import _parse
from app.core.api_helpers.main import _parse_street


def test_parse_name():
    pass


def test_parse_address():
    #  Zip-code, штат, город, улица, дом, номер аппартаментов
    addresess = [
        '73156, CA, SF, Spear street',
        'California, San-Francisco, 123 apt, Lincoln dr.',
        '46060, IN, Noblesville, Lafayette Drive, 162',
    ]
    excpected = [
        {
            'zip_code': '73156',
            'state': 'CA',
            'city': 'SF',
            'street': 'Spear street',
            'apt_num': ''
        },
        {
            'zip_code': '',
            'state': 'California',
            'city': 'San-Francisco',
            'street': 'Lincoln dr.',
            'apt_num': '123'
        },
        {
            'zip_code': '46060',
            'state': 'IN',
            'city': 'Noblesville',
            'street': 'Lafayette Drive',
            'apt_num': '162'
        },

    ]
    for address, check in zip(addresess, excpected):
        res = _parse_street(address, None)
        assert res
        assert res == check


def test_parse_date():
    dates = [
        'December 10th 2010',
        '23/01/2007',
        'September 15th 1990'
    ]
    excpected = [
        '10/12/2010',
        '23/01/2007',
        '15/09/1990'
    ]
    for address, check in zip(dates, excpected):
        res = _parse(address)
        assert res
        assert res['dates'][0] == check
